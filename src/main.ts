import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueMapbox from "vue-mapbox-ts"

createApp(App)
.use(store)
.use(router)
.use(VueMapbox)
.mount('#app')
